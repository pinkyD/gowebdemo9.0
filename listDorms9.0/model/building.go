package model

type Building struct {
	BuildingID   int    `json:"building_id"`
	BuildingName string `json:"building_name"`
}
