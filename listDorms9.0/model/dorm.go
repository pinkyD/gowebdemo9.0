package model

type Dorm struct {
	DormID            int    `json:"dorm_id"`
	UnitID            int    `json:"unit_id"`
	DormName          string `json:"dorm_name"`
	Gender            string `json:"gender"`
	TotalBedCount     int    `json:"total_bed_count"`
	AvailableBedCount int    `json:"available_bed_count"`
	BrokenBedCount    int    `json:"broken_bed_count"`
}
