package model

type Unit struct {
	UnitID     int    `json:"unit_id"`
	BuildingID int    `json:"building_id"`
	UnitName   string `json:"unit_name"`
}
