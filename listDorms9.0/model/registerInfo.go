package model

// 结构体标签（Struct Tag）:在转换成其它数据格式的时候，会使用其中特定的字段作为键值
type RegisterInfo struct {
	ID        int    `json:"id"`
	StudentID string `json:"student_id"`
	Name      string `json:"name"`
	Gender    string `json:"gender"`
	Mobile    string `json:"mobile"`
	// Mail               string `json:"mail"`
	Password string `json:"password"`
	IsDelete bool   `json:"is_del"`
	// AuthenticationCode string `json:"authentication_code"`
}
