package dao

import (
	"listDorms9.0/model"
	"listDorms9.0/utils"
)

func GetAuthenticationByUserID(userID int) (auth *model.Authentication) {
	sqlStr := "select uid, authentication_code from user_authentication_code where uid = ?"
	row := utils.CombineDb.QueryRow(sqlStr, userID)
	auth = &model.Authentication{}
	err := row.Scan(&auth.UserID, &auth.Code)
	if err != nil {
		return nil
	}
	return auth
}
