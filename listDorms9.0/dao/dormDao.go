package dao

import (
	"listDorms9.0/model"
	"listDorms9.0/utils"
)

func GetDorms() ([]*model.Dorm, error) {
	sql := "select unit_id, dorm_name, gender, total_bed_count, available_bed_count, broken_bed_count from dorm"
	rows, err := utils.CombineDb.Query(sql)
	if err != nil {
		return nil, err
	}

	var dorms []*model.Dorm
	for rows.Next() {
		dorm := &model.Dorm{}
		rows.Scan(&dorm.UnitID, &dorm.DormName, &dorm.Gender, &dorm.TotalBedCount, &dorm.AvailableBedCount, &dorm.BrokenBedCount)
		dorms = append(dorms, dorm)
	}
	return dorms, nil
}
