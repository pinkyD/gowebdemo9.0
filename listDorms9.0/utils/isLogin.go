package utils

import (
	"net/http"

	"listDorms9.0/model"
)

//IsLogin 判断用户是否已经登录 false 没有登录 true 已经登录
func IsLogin(r *http.Request) (isLogin bool, token string, claims *model.Claims) {
	//根据Cookie的name获取Cookie
	cookie, _ := r.Cookie("dorm_user")
	if cookie != nil {
		// 获取Cookie的value
		token := cookie.Value
		// 解析Token
		claims, _ := ParseToken(token)

		// 在数据库中查询此学号是否已注册
		var registerInfo model.RegisterInfo
		CombineDb.QueryRow("SELECT id FROM user WHERE student_id=?", claims.StudentID).Scan(&registerInfo.ID)
		if registerInfo.ID != 0 {
			//已经登录
			return true, token, claims
		}
	}
	//没有登录
	return false, "请先登录", nil
}
