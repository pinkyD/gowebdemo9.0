package controller

import (
	"net/http"
	"strconv"

	"listDorms9.0/dao"
	"listDorms9.0/model"
	"listDorms9.0/utils"
)

func ListDorms(w http.ResponseWriter, r *http.Request) {
	// 判断是否登录
	flag, _, claims := utils.IsLogin(r)
	if !flag {
		model.Response(w, false, 500, "请先登录", nil)
		return
	}

	// fmt.Fprintf(w, "DormName\tGender\tTotalBedCount\tAvailableBedCount\tBrokenBedCount\n")
	dorms, _ := dao.GetDorms()

	// 各楼各性别空闲床位统计，下标1-5依次代表5、8、9、13、14号楼
	totalMaleBedCount := make(map[string]int)
	totalFemaleBedCount := make(map[string]int)

	for _, dorm := range dorms {
		// 各楼各性别空闲床位统计
		switch dorm.UnitID {
		case 1: // 5号楼男生空床位
			totalMaleBedCount["Building 5 MALE"] += dorm.AvailableBedCount
		case 2: // 5号楼女生空床位
			totalFemaleBedCount["Building 5 FEMALE"] += dorm.AvailableBedCount
		case 3: // 8号楼男生空床位
			totalMaleBedCount["Building 8 MALE"] += dorm.AvailableBedCount
		case 4: // 8号楼女生空床位
			totalFemaleBedCount["Building 8 FEMALE"] += dorm.AvailableBedCount
		case 5: // 9号楼男生空床位
			totalMaleBedCount["Building 9 MALE"] += dorm.AvailableBedCount
		case 6: // 9号楼女生空床位
			totalFemaleBedCount["Building 9 FEMALE"] += dorm.AvailableBedCount
		case 7: // 13号楼男生空床位
			totalMaleBedCount["Building 13 MALE"] += dorm.AvailableBedCount
		case 8: // 13号楼女生空床位
			totalFemaleBedCount["Building 13 FEMALE"] += dorm.AvailableBedCount
		case 9: // 14号楼男生空床位
			totalMaleBedCount["Building 14 MALE"] += dorm.AvailableBedCount
		case 10: // 14号楼女生空床位
			totalFemaleBedCount["Building 14 FEMALE"] += dorm.AvailableBedCount
		}
		// 打印输出每个dorm的床位情况
		// fmt.Fprintf(w, "%v\t\t%v\t\t%v\t\t\t\t%v\t\t\t\t\t%v\n",
		// 	dorm.DormName, dorm.Gender, dorm.TotalBedCount, dorm.AvailableBedCount, dorm.BrokenBedCount)
	}
	data := make(map[string]string)
	// fmt.Println()
	// 输出每栋楼男生空床位总数
	// fmt.Fprintf(w, "Total MALE bed count in each building:\n")
	for key, value := range totalMaleBedCount {
		// fmt.Fprintf(w, "%v: \t%v\n", key, value)
		data[key] = strconv.Itoa(value)
	}
	// fmt.Println()
	// 输出每栋楼女生空床位总数
	// fmt.Fprintf(w, "Total FEMALE bed count in each building:\n")
	for key, value := range totalFemaleBedCount {
		// fmt.Fprintf(w, "%v: \t%v\n", key, value)
		data[key] = strconv.Itoa(value)
	}

	code := dao.GetAuthenticationByUserID(claims.UserID)
	data["code"] = code.Code

	model.Response(w, true, 200, "成功获取空宿舍信息", data)
}
