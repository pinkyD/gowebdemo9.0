package controller

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"regexp"

	"register9.0/model"
	"register9.0/utils"
)

func Register(w http.ResponseWriter, r *http.Request) {
	var registerInfo model.RegisterInfo
	// 解析请求
	switch r.Method {
	case http.MethodPost:
		dec := json.NewDecoder(r.Body)
		err := dec.Decode(&registerInfo)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
	// 判断格式
	// reg2, _ := regexp.MatchString(`^[^0-9][\w_]{3,11}`, registerInfo.Name) // 用户名必须是4-12位字母、数字或下划线，不能以数字开头
	reg1, _ := regexp.MatchString(`^2101210\d{3}`, registerInfo.StudentID) // 学号：必须为10位数字，且由2101210开头
	reg2, _ := regexp.MatchString(`^[A-Za-z]+$`, registerInfo.Name)        // 姓名：必须全由英文字母组成
	reg3, _ := regexp.MatchString(`^[\w_]{6,20}`, registerInfo.Password)   // 密码：必须是6-20位的字母、数字或下划线
	reg4, _ := regexp.MatchString(`^[FM]{1}`, registerInfo.Gender)         // 性别：只能为1位，且只能为F/M
	reg5, _ := regexp.MatchString(`^1\d{10}`, registerInfo.Mobile)         // 手机号：为1开头的11位数字
	// reg6, _ := regexp.MatchString(`^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$`, registerInfo.Mail) // 邮箱

	if !(reg1 && reg2 && reg3 && reg4 && reg5) {
		// 格式不正确
		if !reg1 {
			model.Response(w, 500, "学号格式不正确", []string{})
		}

		if !reg2 {
			model.Response(w, 500, "姓名需全为英文字符", []string{})
		}

		if !reg3 {
			model.Response(w, 500, "密码必须由6-20位的字母、数字或下划线组成", []string{})
		}

		if !reg4 {
			model.Response(w, 500, "请选择性别", []string{})
		}

		if !reg5 {
			model.Response(w, 500, "手机号格式错误", []string{})
		}

		// if !reg6 {
		// 	model.Response(w, 500, "邮箱格式错误", []string{})
		// }
		return
	} else {
		//加密
		registerInfo.Password = fmt.Sprintf("%x", md5.Sum([]byte(registerInfo.Password)))
		//添加用户
		utils.CombineDb.QueryRow("SELECT id FROM user_information WHERE student_id=?", registerInfo.StudentID).Scan(&registerInfo.ID)
		if registerInfo.ID == 0 {
			// 注册时，将"该用户是否被删除"字段初始化为false
			registerInfo.IsDelete = false

			// 往user_infomation表中添加数据
			sqlStr1 := "insert into user_information(student_id,name,gender,mobile) values(?,?,?,?)"
			_, err1 := utils.CombineDb.Exec(sqlStr1, registerInfo.StudentID, registerInfo.Name, registerInfo.Gender, registerInfo.Mobile)

			if err1 != nil {
				model.Response(w, 500, "注册失败！", []string{"error1:", err1.Error()})
			} else {
				// 生成6位认证码（随机字符串）
				// registerInfo.AuthenticationCode = utils.GetRandomString(6)
				// fmt.Println(user.AuthenticationCode)

				// 往user表中添加数据
				sqlStr2 := "insert into user(student_id,password,is_delete) values(?,?,?)"
				_, err2 := utils.CombineDb.Exec(sqlStr2, registerInfo.StudentID, registerInfo.Password, registerInfo.IsDelete)

				// 往user_authentication_code表中添加数据(初始认证码为空)
				sqlStr3 := "insert into user_authentication_code(authentication_code) values(?)"
				_, err3 := utils.CombineDb.Exec(sqlStr3, "")

				if err2 != nil || err3 != nil {
					model.Response(w, 500, "注册失败！", []string{"error2:", err2.Error()})
				} else {
					utils.CombineDb.QueryRow("SELECT id FROM user WHERE student_id=?", registerInfo.StudentID).Scan(&registerInfo.ID)

					// 往user表和user_authentication_code表中添加uid
					_, err4 := utils.CombineDb.Exec("update user set uid=? where student_id=?", registerInfo.ID, registerInfo.StudentID)
					_, err5 := utils.CombineDb.Exec("update user_authentication_code set uid=? where id=?", registerInfo.ID, registerInfo.ID)

					if err4 != nil || err5 != nil {
						model.Response(w, 500, "注册失败", []string{"error4:", err4.Error()})
					} else {
						model.Response(w, 200, "注册成功！", []string{})
					}
				}
			}
		} else {
			rst := &model.Result{
				Code: 500,
				Msg:  "用户已注册",
				Data: []string{},
			}
			response, _ := json.Marshal(rst)
			fmt.Fprintln(w, string(response))
		}
	}
}
