package utils

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

var (
	CombineDb *sql.DB
	DormDb    *sql.DB
	UserDb    *sql.DB
	err       error
)

const (
	// 容器数据库
	Container_combine_DB_Driver = "root:123456@tcp(combine_db)/combine_db"
	// Container_combine_DB_Driver = "root:123456@tcp(47.100.117.51:3309)/combine_db"
	Container_dorm_DB_Driver    = "root:123456@tcp(47.100.117.51:3308)/dorm_db"
	Container_user_DB_Driver    = "root:123456@tcp(47.100.117.51:3307)/user_db"

	// 服务器数据库
	Server_DB_Driver = "root:112728@tcp(47.100.117.51:3306)/db"
)

// 初始化数据库连接，init()方法系统会在动在main方法之前执行，无参数，不可被调用。
func init() {
	CombineDb, err = sql.Open("mysql", Container_combine_DB_Driver) // 容器数据库combine
	// DormDb, err = sql.Open("mysql", Container_dorm_DB_Driver) // 容器数据库dorm
	// UserDb, err = sql.Open("mysql", Container_user_DB_Driver) // 容器数据库user

	// Db, err = sql.Open("mysql", Server_DB_Driver) // 服务器数据库

	if err != nil {
		panic(err.Error())
	}
}
