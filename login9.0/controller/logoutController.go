package controller

import (
	"net/http"

	"login9.0/model"
)

func Logout(w http.ResponseWriter, r *http.Request) {
	//获取Cookie
	cookie, _ := r.Cookie("dorm_user")
	if cookie != nil {
		//设置cookie失效
		cookie.MaxAge = -1
		//将修改之后的cookie发送给浏览器
		http.SetCookie(w, cookie)
	}
	//返回成功消息
	model.Response(w, 200, "退出成功", nil)
}
