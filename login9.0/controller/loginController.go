package controller

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strconv"

	"login9.0/dao"
	"login9.0/model"
	"login9.0/utils"
)

func Login(w http.ResponseWriter, r *http.Request) {
	var loginInfo model.LoginInfo
	// 解析请求
	switch r.Method {
	case http.MethodPost:
		dec := json.NewDecoder(r.Body)
		err := dec.Decode(&loginInfo)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	// 判断格式
	reg1, _ := regexp.MatchString(`^2101210\d{3}`, loginInfo.StudentID) // 学号：必须为10位数字，且由2101210开头
	reg3, _ := regexp.MatchString(`^[\w_]{6,20}`, loginInfo.Password)   // 密码：必须是6-20位的字母、数字或下划线

	if !(reg1 && reg3) {
		// 格式不正确
		if !reg1 {
			model.Response(w, 500, "学号格式不正确", []string{})
		}

		if !reg3 {
			model.Response(w, 500, "密码必须由6-20位的字母、数字或下划线组成", []string{})
		}

		return
	} else {
		//格式正确，查询手机号和密码是否匹配
		loginInfo.Password = fmt.Sprintf("%x", md5.Sum([]byte(loginInfo.Password))) // 密码明文转密文
		loginInfo, err := dao.CheckPassword(loginInfo.StudentID, loginInfo.Password)
		if err != nil {
			//手机号或密码不正确
			model.Response(w, 500, "登陆失败，学号或密码不正确，或用户未注册", []string{})
			return
		} else {
			//用户名和密码正确
			stu := dao.GetStudentByStudentID(loginInfo.StudentID) // 通过studentID查找user_information表中用户信息
			token, err := utils.GenerateToken(stu.UID, stu.StudentID)
			if err != nil {
				model.Response(w, 500, "未成功生成Token", []string{})
				return
			}

			// Token储存方式1:将Token存进cookie里
			cookie1 := http.Cookie{
				Name:     "dorm_user",
				Value:    token,
				HttpOnly: false,
			}
			//将cookie发送给浏览器
			http.SetCookie(w, &cookie1)

			// Token储存方式2:将Token存进localstorage里(在前端实现)

			// 判断用户的选宿舍资格
			if !isQualifiedToChooseDorm(loginInfo.StudentID) {
				model.Response(w, 500, "无选宿舍资格", []string{})
			}

			// 获取认证码：认证码不为6位，则重新获取认证码
			auth := dao.GetAuthenticationByUserID(stu.UID)
			code := auth.Code
			if len(code) != 6 {
				code, err = dao.AddAuthenticationCode(stu.UID)
				if err != nil {
					model.Response(w, 500, "认证码添加失败", nil)
					return
				}
			}

			// 成功登陆，并获取认证码
			model.Response(w, 200, "登陆成功！", []string{code, token})
			return
		}
	}
}

// 判断用户是否有选宿舍资格：学号在2101210000-2101210999之间的用户有资格
func isQualifiedToChooseDorm(studentID string) bool {
	IntID, _ := strconv.Atoi(studentID)
	if IntID > 2101210999 || IntID < 2101210000 {
		return false
	}
	return true
}
