package dao

import (
	"login9.0/model"
	"login9.0/utils"
)

func CheckPassword(studentID string, password string) (*model.LoginInfo, error) {
	sqlStr := "select student_id, password from user where student_id = ? and password = ?"
	loginInfo := &model.LoginInfo{}
	row := utils.CombineDb.QueryRow(sqlStr, studentID, password)
	err := row.Scan(&loginInfo.StudentID, &loginInfo.Password)
	if err != nil {
		return loginInfo, err
	}
	return loginInfo, nil
}
