package dao

import (
	"math/rand"
	"time"

	"login9.0/model"
	"login9.0/utils"
)

func GetRandomString(n int) string {
	str := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	bytes := []byte(str)
	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < n; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}

// 通过查找uid所在行来插入认证码
func AddAuthenticationCode(userID int) (code string, err error) {
	code = GetRandomString(6)
	sqlStr := "update user_authentication_code set authentication_code = ? where uid = ?"
	_, err = utils.CombineDb.Exec(sqlStr, code, userID)
	return code, err
}

func GetAuthenticationByUserID(userID int) (auth *model.Authentication) {
	sqlStr := "select uid, authentication_code from user_authentication_code where uid = ?"
	row := utils.CombineDb.QueryRow(sqlStr, userID)
	auth = &model.Authentication{}
	err := row.Scan(&auth.UserID, &auth.Code)
	if err != nil {
		return nil
	}
	return auth
}

func GetAuthenticationByCode(code string) (auth *model.Authentication) {
	sqlStr := "select id, uid, authentication_code from user_authentication_code where authentication_code = ?"
	row := utils.CombineDb.QueryRow(sqlStr, code)
	auth = &model.Authentication{}
	err := row.Scan(&auth.ID, &auth.UserID, &auth.Code)
	if err != nil {
		return nil
	}
	return auth
}
