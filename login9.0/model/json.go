package model

import (
	"encoding/json"
	"net/http"
)

type Result struct {
	Code int      `json:"status"`
	Msg  string   `json:"msg"`
	Data []string `json:"data"`
}

// json化结果集
func Response(w http.ResponseWriter, code int, message string, data []string) {
	w.Header().Set("Content-Type", "application/json")
	rst := &Result{
		Code: code,
		Msg:  message,
		Data: data,
	}
	response, _ := json.Marshal(rst)
	w.Write(response)
}
