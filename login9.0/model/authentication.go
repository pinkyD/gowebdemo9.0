package model

type Authentication struct {
	ID     int    `json:"id"`
	UserID int    `json:"uid"`
	Code   string `json:"authentication_code"`
}
