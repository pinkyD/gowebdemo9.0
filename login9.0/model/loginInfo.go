package model

// 结构体标签（Struct Tag）:在转换成其它数据格式的时候，会使用其中特定的字段作为键值
type LoginInfo struct {
	StudentID string `json:"student_id"`
	Password  string `json:"password"`
}
