package utils

import (
	"time"

	"github.com/dgrijalva/jwt-go"

	"login9.0/model"
)

var jwtSecret = []byte("DengHanzi") // jwt密钥

// 生成 token
func GenerateToken(uid int, studentID string) (string, error) {
	nowTime := time.Now()
	expireTime := nowTime.Add(30000 * time.Second)
	issuer := "frank"
	claims := model.Claims{
		UserID:    uid,
		StudentID: studentID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expireTime.Unix(),
			Issuer:    issuer,
		},
	}

	tokenClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err := tokenClaims.SignedString(jwtSecret)

	return token, err
}

// 解析 token
func ParseToken(token string) (*model.Claims, error) {
	tokenClaims, err := jwt.ParseWithClaims(token, &model.Claims{}, func(token *jwt.Token) (interface{}, error) {
		return jwtSecret, nil
	})
	if err != nil {
		return nil, err
	}

	if tokenClaims != nil {
		if claims, ok := tokenClaims.Claims.(*model.Claims); ok && tokenClaims.Valid {
			return claims, nil
		}
	}
	return nil, err
}
