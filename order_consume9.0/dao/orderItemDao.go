package dao

import (
	"order_consume9.0/model"
	"order_consume9.0/utils"
)

func AddOrderItem(orderItem *model.OrderItem) error {
	sql := "insert into order_item(order_id, student_id) values(?,?)"
	_, err := utils.CombineDb.Exec(sql, orderItem.OrderID, orderItem.StudentID)
	return err
}

func GetItemsByOrderID(OrderID int) (orderItems []*model.OrderItem) {
	sqlStr := "select item_id, order_id, student_id from order_item where order_id = ?"
	rows, err := utils.CombineDb.Query(sqlStr, OrderID)
	if err != nil {
		return nil
	}
	for rows.Next() {
		item := &model.OrderItem{}
		err = rows.Scan(&item.ID, &item.OrderID, &item.StudentID)
		if err != nil {
			return orderItems
		}
		orderItems = append(orderItems, item)
	}
	return orderItems
}
