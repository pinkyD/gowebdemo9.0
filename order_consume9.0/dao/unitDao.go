package dao

import (
	"order_consume9.0/model"
	"order_consume9.0/utils"
)

func GetUnitsByBuilding(BuildingID int) (units []*model.Unit) {
	sqlStr := "select unit_id, building_id, unit_name from unit where building_id = ?"
	rows, err := utils.CombineDb.Query(sqlStr, BuildingID)
	if err != nil {
		return nil
	}
	for rows.Next() {
		unit := &model.Unit{}
		err = rows.Scan(&unit.ID, &unit.BuildingID, &unit.Name)
		if err != nil {
			return units
		}
		units = append(units, unit)
	}
	return units
}

func GetUnitByID(ID int) (unit *model.Unit) {
	sqlStr := "select unit_id, building_id, unit_name from unit where unit_id = ?"
	row := utils.CombineDb.QueryRow(sqlStr, ID)
	unit = &model.Unit{}
	err := row.Scan(&unit.ID, &unit.BuildingID, &unit.Name)
	if err != nil {
		return nil
	}
	return unit
}
