package dao

import (
	"order_consume9.0/model"
	"order_consume9.0/utils"
)

func AddOrder(order *model.Order) error {
	sql := "insert into order_list(committer_id, building_id, gender, student_count, is_success, commit_time) values(?,?,?,?,?,?)"
	_, err := utils.CombineDb.Exec(sql, order.CommitterID, order.BuildingID, order.Gender, order.StudentCount, order.IsSuccess, order.CommitTime)
	if err != nil {
		return err
	}
	return nil
}

func GetOrderByCommitTime(commitTime string) (order *model.Order, err error) {
	sql := "select order_id, committer_id, building_id, gender, student_count, is_success, commit_time from order_list where commit_time = ?"
	row := utils.CombineDb.QueryRow(sql, commitTime)
	order = &model.Order{}
	err = row.Scan(&order.ID, &order.CommitterID, &order.BuildingID, &order.Gender, &order.StudentCount, &order.IsSuccess, &order.CommitTime)
	if err != nil {
		return nil, err
	}
	return order, nil
}

func GetAllOrders() (orders []*model.Order) {
	sql := "select order_id, committer_id, building_id, gender, student_count, is_success, commit_time from order_list"
	rows, err := utils.CombineDb.Query(sql)
	if err != nil {
		return nil
	}
	for rows.Next() {
		order := &model.Order{}
		err := rows.Scan(&order.ID, &order.CommitterID, &order.BuildingID, &order.Gender, &order.StudentCount, &order.IsSuccess, &order.CommitTime)
		if err != nil {
			return orders
		}
		orders = append(orders, order)
	}
	return orders
}

func GetOrderByOrderID(orderID int) (order *model.Order, err error) {
	sql := "select order_id, committer_id, building_id, gender, student_count, is_success, commit_time from order_list where order_id=? and is_success = 0"
	row := utils.CombineDb.QueryRow(sql, orderID)
	order = &model.Order{}
	err = row.Scan(&order.ID, &order.CommitterID, &order.BuildingID, &order.Gender, &order.StudentCount, &order.IsSuccess, &order.CommitTime)
	if err != nil {
		return nil, err
	}
	return order, nil
}

func GetOrder(timeStr string) (order *model.Order, err error) {
	sql := "select order_id, committer_id, building_id, gender, student_count, is_success, commit_time from order_list where commit_time=? and is_success = 0 ORDER BY commit_time ASC" // 按提交时间升序
	row := utils.CombineDb.QueryRow(sql, timeStr)
	order = &model.Order{}
	err = row.Scan(&order.ID, &order.CommitterID, &order.BuildingID, &order.Gender, &order.StudentCount, &order.IsSuccess, &order.CommitTime)
	if err != nil {
		return nil, err
	}
	return order, nil
}

// func GetUnprocessedOrders(timeStr string) (orders []*model.Order) {
// 	sql := "select order_id, committer_id, building_id, gender, student_count, is_success, commit_time from order_list where commit_time<=? and is_success = 0 ORDER BY commit_time ASC" // 按提交时间升序
// 	rows, err := utils.CombineDb.Query(sql, timeStr)
// 	if err != nil {
// 		return nil
// 	}
// 	for rows.Next() {
// 		order := &model.Order{}
// 		err := rows.Scan(&order.ID, &order.CommitterID, &order.BuildingID, &order.Gender, &order.StudentCount, &order.IsSuccess, &order.CommitTime)
// 		if err != nil {
// 			return orders
// 		}
// 		orders = append(orders, order)
// 	}
// 	return orders
// }

// func GetOrdersByCommitterID(userID int) (orders []*model.Order) {
// 	sql := "select order_id, committer_id, building_id, gender, student_count, is_success, commit_time from order_list where committer_id = ?"
// 	rows, err := utils.CombineDb.Query(sql, userID)
// 	if err != nil {
// 		return nil
// 	}
// 	for rows.Next() {
// 		order := &model.Order{}
// 		err := rows.Scan(&order.ID, &order.CommitterID, &order.BuildingID, &order.Gender, &order.StudentCount, &order.IsSuccess, &order.CommitTime)
// 		if err != nil {
// 			return orders
// 		}
// 		orders = append(orders, order)
// 	}
// 	return orders
// }

func UpdateOrderState(orderID int, isSuccess bool) error {
	//写sql语句
	sql := "update order_list set is_success = ? where order_id = ?"
	//执行
	_, err := utils.CombineDb.Exec(sql, isSuccess, orderID)
	return err
}
