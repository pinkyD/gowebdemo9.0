package dao

import (
	"order_consume9.0/model"
	"order_consume9.0/utils"
)

// 获取单元、性别、床位数符合要求的宿舍
func GetAvailableDorms(unitID, studentCount int, gender string) (dorms []*model.Dorm) {
	sqlStr := "select dorm_id, unit_id, dorm_name, gender, total_bed_count, available_bed_count, broken_bed_count from dorm where unit_id = ? and gender =? and available_bed_count >= ?"
	rows, err := utils.CombineDb.Query(sqlStr, unitID, gender, studentCount)
	if err != nil {
		return nil
	}
	for rows.Next() {
		dorm := &model.Dorm{}
		err = rows.Scan(&dorm.ID, &dorm.UnitID, &dorm.Name, &dorm.Gender, &dorm.TotalBedCount, &dorm.AvailableBedCount, &dorm.BrokenBedCount)
		if err != nil {
			return dorms
		}

		dorms = append(dorms, dorm)
	}
	return dorms
}

func GetDormByID(ID int) (dorm *model.Dorm) {
	sqlStr := "select dorm_id, unit_id, dorm_name, gender, total_bed_count, available_bed_count, broken_bed_count from dorm where dorm_id = ?"
	row := utils.CombineDb.QueryRow(sqlStr, ID)
	dorm = &model.Dorm{}
	err := row.Scan(&dorm.ID, &dorm.UnitID, &dorm.Name, &dorm.Gender, &dorm.TotalBedCount, &dorm.AvailableBedCount, &dorm.BrokenBedCount)
	if err != nil {
		return nil
	}
	return dorm
}
func UpdateDormAvailableBeds(dormID, availableBeds int) (err error) {
	sqlStr := "update dorm set available_bed_count = ? where dorm_id = ?"
	_, err = utils.CombineDb.Exec(sqlStr, availableBeds, dormID)
	if err != nil {
		return err
	}
	return nil
}
