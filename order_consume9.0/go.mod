module order_consume9.0

go 1.17

require (
	github.com/garyburd/redigo v1.6.3
	github.com/go-sql-driver/mysql v1.6.0
	github.com/rabbitmq/amqp091-go v1.2.0
)
