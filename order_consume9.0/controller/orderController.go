package controller

import (
	"fmt"
	"log"
	"strconv"

	"github.com/garyburd/redigo/redis"

	"order_consume9.0/dao"
	"order_consume9.0/model"
)

func GetAndProcessOrderInQueue(orderID int) (string, bool) {
	order, err := dao.GetOrderByOrderID(orderID)
	failOnError(err, "获取订单失败")
	// 判断所有申请人是否已选宿舍
	orderItems := dao.GetItemsByOrderID(order.ID)

	// 连接redis
	conn, err := redis.Dial("tcp", "redis:6379")
	// conn, err := redis.Dial("tcp", "47.100.117.51:6379")
	if err != nil {
		fmt.Println("connect redis error:", err)
		return err.Error(), false
	}
	defer conn.Close()

	for _, orderItem := range orderItems {
		// 在redis缓存中查询该studentID是否已选宿舍
		dormID, err := redis.String(conn.Do("GET", orderItem.StudentID))
		if err != nil {
			fmt.Println("redis get error:", err) // 没查到时会有get error
		} else {
			fmt.Printf("Get %s: %s \n", orderItem.StudentID, dormID)
			return orderItem.StudentID + "已选宿舍", false
		}
		// dorm := dao.GetStuDormByStudentID(orderItem.StudentID)
		// if dorm != nil {
		// 	return orderItem.StudentID + "已选宿舍", false
		// }
	}
	// 获取满足订单条件的宿舍列表
	var dorms []*model.Dorm
	// 获取该楼号下的所有单元
	units := dao.GetUnitsByBuilding(order.BuildingID)
	for _, unit := range units {
		dorms = append(dorms, dao.GetAvailableDorms(unit.ID, order.StudentCount, order.Gender)...) // 不定参数
	}
	// 如果宿舍列表不为空
	if len(dorms) > 0 {
		dorm := dorms[0] // 顺序分配
		availableBeds := dorm.AvailableBedCount - order.StudentCount
		// 更新宿舍空床数
		dao.UpdateDormAvailableBeds(dorm.ID, availableBeds)
		// 将选宿舍信息加入学生宿舍表
		items := dao.GetItemsByOrderID(order.ID)
		for _, item := range items {
			dao.AddStudentIntoDorm(item.StudentID, dorm.ID)

			// 将已选宿舍的studentID存入redis缓存
			_, err = conn.Do("SET", item.StudentID, dorm.ID)
			if err != nil {
				fmt.Println("redis set error:", err)
			} else {
				fmt.Printf("Set %s: %s \n", item.StudentID, strconv.Itoa(dorm.ID))
			}
		}
		// 更新订单状态
		dao.UpdateOrderState(order.ID, true)
	}
	return "选宿舍成功", true

}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}
