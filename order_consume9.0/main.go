package main

import (
	"fmt"
	"log"
	"strconv"

	amqp "github.com/rabbitmq/amqp091-go"

	"order_consume9.0/controller"
)

func main() {
	// 处理消息（消息队列守护进程）
	conn, err := amqp.Dial("amqp://user01:123456@rabbitmq/")
	for {
		conn, err = amqp.Dial("amqp://user01:123456@rabbitmq/")
		if err == nil {
			break
		}
	}
	// conn, err := amqp.Dial("amqp://user01:123456@47.100.117.51:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"hello", // name
		false,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

	// 消费者
	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			log.Printf("Received a message: %s", d.Body)
			// 获取订单ID
			orderID, err := strconv.Atoi(string(d.Body))
			failOnError(err, "格式转换出错")
			// 处理订单
			msg, isSuccess := controller.GetAndProcessOrderInQueue(orderID)
			if !isSuccess {
				fmt.Println(msg)
			}
			fmt.Println("成功处理订单：" + string(d.Body))
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}
