const express = require('express');
const ejs = require('ejs');
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser');
const request = require("request");
// const cors = require('cors'); 

const port = 3000;



app = express();

app.set('views', './views');
app.engine('html', ejs.__express);
app.set('view engine', 'html');

// app.use(express.static('public'))
app.use(bodyParser.json())
app.use(cookieParser())

// 解决跨域
// 方法一：使用cors依赖
// app.use(cors({
//   origin: [
//     'http://47.100.117.51:3000/register',
//     'http://47.100.117.51:3000/login',
//     'http://47.100.117.51:3000/list',
//     'http://47.100.117.51:3000/order',
//     'http://47.100.117.51:3000/result'
//   ],//可设置多个跨域
//   credentials: true//允许客户端携带验证信息
// }))
// 方法二：直接设置
// app.all('*',function (req, res, next) {
//   res.header('Access-Control-Allow-Origin', 'http://47.100.117.51:3000/login', 'http://47.100.117.51:3000/register', 'http://47.100.117.51:3000/order', 'http://47.100.117.51:3000/list', 'http://47.100.117.51:3000/result');//可设置多个跨域
//   res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
//   res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
//   res.header('Access-Control-Allow-Credentials',true)//允许客户端携带验证信息
//  　next();　
//  });


// 提供页面
app.get('/register', function (request, response) {
  response.render('register');
});

app.get('/login', function (request, response) {
  response.render('login');
});

app.get('/welcome', function (request, response) {
  response.render('welcome');
});

// app.get('/list', function (request, response) {
//   response.render('list');
// });

app.get('/order', function (request, response) {
  response.render('order');
});

app.get('/result', function (request, response) {
  response.render('result');
});


// 注册
app.post('/register', function (req, res) {
  var url = "http://register:8081/register";
  var requestData = {
    student_id: req.body.student_id,
    name: req.body.name,
    password: req.body.password,
    gender: req.body.gender,
    mobile: req.body.mobile
  };
  request({
    url: url,
    method: "POST",
    json: true,
    headers: { "content-type": "application/json", },
    body: requestData
  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body) // 请求成功的处理逻辑
      res.json(body)
    }
  })
})

// 登录
app.post('/login', function (req, res) {
  var url = "http://login:8082/login";
  var requestData = {
    student_id: req.body.student_id,
    password: req.body.password
  };
  request({
    url: url,
    method: "POST",
    json: true,
    headers: { "content-type": "application/json"},
    body: requestData
  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body) // 请求成功的处理逻辑
      // 使用响应头验证token
      // res.setHeader('Authorization', response.headers.authorization);
      if (response.headers['set-cookie'] != null) {
        res.setHeader('Set-Cookie', response.headers['set-cookie']);
      }
      console.log(response.headers)
      res.json(body)
    }
  })
})

// 登出
app.post('/logout', function (req, res) {
  var url = "http://login:8082/logout";
  request({
    url: url,
    method: "POST",
    json: true,
    headers: { 
      "content-type": "application/json",
      "Cookie": 'dorm_user='+req.cookies.dorm_user
    }
  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body) // 请求成功的处理逻辑
      // 清除cookie
      if (response.headers['set-cookie'] != null) {
        res.setHeader('Set-Cookie', response.headers['set-cookie']);
      }
      console.log(response.headers)
      res.json(body)
    }
  })
})

// 查询空床位
app.post('/list', function (req, res) {
  var url = "http://list:8083/list";
  request({
    url: url,
    method: "POST",
    json: true,
    headers: { 
      "content-type": "application/json",
      "Cookie": 'dorm_user='+req.cookies.dorm_user
    }
  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body) // 请求成功的处理逻辑
      res.json(body)
    }
  })
})

// 选宿舍
app.post('/chooseDorm', function (req, res) {
  var url = "http://order_generate:8084/chooseDorm";
  var requestData = {
    building_name: req.body.building_name,
    roomate1_code: req.body.roomate1_code,
    roomate2_code: req.body.roomate2_code,
    roomate3_code: req.body.roomate3_code,
    roomate4_code: req.body.roomate4_code
  };

  console.log(req.cookies.dorm_user)

  request({
    url: url,
    method: "POST",
    json: true,
    headers: { 
      "content-type": "application/json",
      "Cookie": 'dorm_user='+req.cookies.dorm_user
    },
    body: requestData
  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body) // 请求成功的处理逻辑
      res.json(body)
    }
  })
})

// 查询选宿舍结果
app.post('/result', function (req, res) {
  var url = "http://order_generate:8084/result";
  request({
    url: url,
    method: "POST",
    json: true,
    headers: { "content-type": "application/json"},
    headers: { 
      "content-type": "application/json",
      "Cookie": 'dorm_user='+req.cookies.dorm_user
    }
  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body) // 请求成功的处理逻辑
      res.json(body)
    }
  })
})

const server = app.listen(port, function () {
  console.log(`Example app listening at http://localhost:${port}`)
})
