package controller

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"order_generate9.0/dao"
	"order_generate9.0/model"
	"order_generate9.0/utils"
)

// ChooseDorm 提交选宿舍订单
func ChooseDorm(w http.ResponseWriter, r *http.Request) {
	// 判断是否登录
	flag, token, claims := utils.IsLogin(r)
	if !flag {
		model.Response(w, false, 500, "请先登录", map[string]string{"token": token})
		return
	}

	// 解析请求
	var orderInfo model.OrderInfo
	switch r.Method {
	case http.MethodPost:
		dec := json.NewDecoder(r.Body)
		err := dec.Decode(&orderInfo)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	// 解析所选楼号
	buildingName := orderInfo.BuildingName
	building, err := dao.GetBuildingByName(buildingName)
	if building == nil { // 用于调试
		model.Response(w, false, 500, "楼号格式错误", map[string]string{"building_name": buildingName, "err": err.Error()})
		return
	}

	var students []*model.Student
	var codeStrs []string = []string{orderInfo.Roomate1Code, orderInfo.Roomate2Code, orderInfo.Roomate3Code, orderInfo.Roomate4Code}
	for _, codeStr := range codeStrs {
		// 获取用户的认证码
		if codeStr != "" {
			auth, _ := dao.GetAuthenticationByCode(codeStr)
			if auth == nil { // 调试代码
				model.Response(w, false, 500, "所绑定用户认证码不存在", map[string]string{"code": codeStr})
				return
			}

			stu, err := dao.GetStudentByUID(auth.UserID)
			if err != nil { // 调试代码
				model.Response(w, false, 500, "GetStudentByUID出错", map[string]string{"err": err.Error()})
				return
			}
			students = append(students, stu)
		}
	}

	// 校验订单中的学生性别是否一致
	for _, stu := range students {
		if stu.Gender != students[0].Gender {
			model.Response(w, false, 500, "所绑定用户性别需一致", nil)
			return
		}
	}

	// 创建生成订单的时间
	timeStr := time.Now().Format("2006-01-02 15:04:05")
	// 生成订单
	order := &model.Order{
		CommitterID:  claims.StudentID,
		BuildingID:   building.ID,
		Gender:       students[0].Gender,
		StudentCount: len(students),
		IsSuccess:    false,
		CommitTime:   timeStr,
	}
	dao.AddOrder(order)
	order, _ = dao.GetOrderByCommitTime(timeStr) // 根据订单创建时间获取orderID

	// 添加订单详情表中条目
	for _, stu := range students {
		orderItem := &model.OrderItem{
			OrderID:   order.ID,
			StudentID: stu.StudentID,
		}
		dao.AddOrderItem(orderItem)
		// fmt.Println("成功添加订单详情")
	}

	// 将订单加入rabbitMQ
	err = utils.SendOrder(order.ID)
	if err != nil {
		model.Response(w, false, 500, "该订单未成功加入消息队列", nil)
	}
	model.Response(w, false, 200, "该订单成功加入消息队列", nil)

	// // 处理订单
	// rstStr, isSuccess := ProcessCurrentOrder(timeStr)
	// if !isSuccess {
	// 	model.Response(w, false, 500, "宿舍申请失败", map[string]string{"失败原因": rstStr})
	// 	return
	// }

	// // 获取结果
	// stuDorm := dao.GetStuDormByStudentID(students[0].StudentID)
	// if stuDorm == nil {
	// 	// TODO: choose dorm
	// 	model.Response(w, false, 500, "选宿舍失败", nil)
	// 	return
	// }
	// dorm := dao.GetDormByID(stuDorm.DormID)
	// unit := dao.GetUnitByID(dorm.UnitID)
	// building = dao.GetBuildingByID(unit.BuildingID)
	// data := map[string]string{
	// 	"Building:": building.Name,
	// 	"Unit:":     unit.Name,
	// 	"Dorm:":     dorm.Name,
	// }
	// for i, stu := range students {
	// 	data["宿舍成员"+strconv.Itoa(i+1)] = stu.StudentID
	// }

	// model.Response(w, true, 200, "宿舍申请成功", data)
}

func GetAndProcessOrderInQueue(orderID int) (string, bool) {
	order, err := dao.GetOrderByOrderID(orderID)
	failOnError(err, "获取订单失败")
	// 判断所有申请人是否已选宿舍
	orderItems := dao.GetItemsByOrderID(order.ID)
	for _, orderItem := range orderItems {
		dorm := dao.GetStuDormByStudentID(orderItem.StudentID)
		if dorm != nil {
			return orderItem.StudentID + "已选宿舍", false
		}
	}
	// 获取满足订单条件的宿舍列表
	var dorms []*model.Dorm
	// 获取该楼号下的所有单元
	units := dao.GetUnitsByBuilding(order.BuildingID)
	for _, unit := range units {
		dorms = append(dorms, dao.GetAvailableDorms(unit.ID, order.StudentCount, order.Gender)...) // 不定参数
	}
	// 如果宿舍列表不为空
	if len(dorms) > 0 {
		dorm := dorms[0] // 顺序分配
		availableBeds := dorm.AvailableBedCount - order.StudentCount
		// 更新宿舍空床数
		dao.UpdateDormAvailableBeds(dorm.ID, availableBeds)
		// 将选宿舍信息加入学生宿舍表
		items := dao.GetItemsByOrderID(order.ID)
		for _, item := range items {
			dao.AddStudentIntoDorm(item.StudentID, dorm.ID)
		}
		// 更新订单状态
		dao.UpdateOrderState(order.ID, true)
	}
	return "选宿舍成功", true

}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

// ProcessCurrentOrder 处理当前订单
// func ProcessCurrentOrder(timeStr string) (string, bool) {
// 	order, _ := dao.GetOrder(timeStr)
// 	// 判断所有申请人是否已选宿舍
// 	orderItems := dao.GetItemsByOrderID(order.ID)
// 	for _, orderItem := range orderItems {
// 		dorm := dao.GetStuDormByStudentID(orderItem.StudentID)
// 		if dorm != nil {
// 			return orderItem.StudentID + "已选宿舍", false
// 		}
// 	}
// 	// 获取满足订单条件的宿舍列表
// 	var dorms []*model.Dorm
// 	// 获取该楼号下的所有单元
// 	units := dao.GetUnitsByBuilding(order.BuildingID)
// 	for _, unit := range units {
// 		dorms = append(dorms, dao.GetAvailableDorms(unit.ID, order.StudentCount, order.Gender)...) // 不定参数
// 	}
// 	// 如果宿舍列表不为空
// 	if len(dorms) > 0 {
// 		dorm := dorms[0] // 顺序分配
// 		availableBeds := dorm.AvailableBedCount - order.StudentCount
// 		// 更新宿舍空床数
// 		dao.UpdateDormAvailableBeds(dorm.ID, availableBeds)
// 		// 将选宿舍信息加入学生宿舍表
// 		items := dao.GetItemsByOrderID(order.ID)
// 		for _, item := range items {
// 			dao.AddStudentIntoDorm(item.StudentID, dorm.ID)
// 		}
// 		// 更新订单状态
// 		dao.UpdateOrderState(order.ID, true)
// 	}
// 	return "选宿舍成功", true
// }

// GetResult 结果查询
func GetResult(w http.ResponseWriter, r *http.Request) {
	// 判断是否登录
	flag, _, claims := utils.IsLogin(r)
	if !flag {
		// TODO: login
		return
	}
	// 获取登录用户的信息
	stu, err := dao.GetStudentByUID(claims.UserID)
	if stu == nil { // 调试代码
		model.Response(w, false, 500, "auth获取失败", map[string]string{"claims.StudentID": claims.StudentID, "err": err.Error()})
		return
	}
	// 判断是否选宿舍成功
	stuDorm := dao.GetStuDormByStudentID(stu.StudentID)
	if stuDorm == nil {
		// TODO: choose dorm
		model.Response(w, false, 500, "没有您的宿舍信息", nil)
		return
	}

	dorm := dao.GetDormByID(stuDorm.DormID)
	unit := dao.GetUnitByID(dorm.UnitID)
	building := dao.GetBuildingByID(unit.BuildingID)

	data := map[string]string{"building": building.Name, "unit": unit.Name, "dorm": dorm.Name}
	model.Response(w, true, 200, "选宿舍成功", data)
}

//ProcessOrder 处理时间早于timeStr发生的订单
//func ProcessOrder(timeStr string) {
//	// 获取待处理订单
//	orders := dao.GetUnprocessedOrdersBefore(timeStr)
//	for _, order := range orders {
//		// 获取满足订单条件的宿舍列表
//		var dorms []*model.Dorm
//		units := dao.GetUnitsByBuilding(order.BuildingID) // 该楼号下的所有单元
//		for _, unit := range units {
//			dorms = append(dorms, dao.GetAvailableDorms(unit.ID, order.StudentCount, order.Gender)...)
//		}
//		// 如果宿舍列表不为空
//		if len(dorms) > 0 {
//			dorm := dorms[0]
//			availableBeds := dorm.AvailableBedCount - order.StudentCount
//			// 更新宿舍空床数
//			dao.UpdateDormAvailableBeds(dorm.ID, availableBeds)
//			// 将选宿舍信息加入学生宿舍表
//			items := dao.GetItemsByOrderID(order.ID)
//			for _, item := range items {
//				dao.AddStudentIntoDorm(item.StudentID, dorm.ID)
//			}
//			// 更新订单状态
//			dao.UpdateOrderState(order.ID, true)
//		} else {
//			dao.UpdateOrderState(order.ID, false)
//		}
//	}
//}
