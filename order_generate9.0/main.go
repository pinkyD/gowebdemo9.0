package main

import (
	"net/http"

	"order_generate9.0/controller"
)

func main() {
	// 选宿舍
	http.HandleFunc("/chooseDorm", controller.ChooseDorm)
	// 查询结果
	http.HandleFunc("/result", controller.GetResult)

	http.ListenAndServe(":8084", nil)
}
