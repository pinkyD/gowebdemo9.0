package utils

import (
	"log"
	"strconv"

	amqp "github.com/rabbitmq/amqp091-go"
)

func SendOrder(orderID int) error {
	conn, err := amqp.Dial("amqp://user01:123456@rabbitmq/")
	// conn, err := amqp.Dial("amqp://user01:123456@47.100.117.51:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"hello", // name
		false,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

	// 将orderID转换为字符串存入队列
	body := strconv.Itoa(orderID)

	// 生产者
	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		})
	failOnError(err, "Failed to publish a message")
	log.Printf(" [x] Sent %s\n", body)

	return err
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}
