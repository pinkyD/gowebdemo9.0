package dao

import (
	"math/rand"
	"time"

	"order_generate9.0/model"
	"order_generate9.0/utils"
)

func GetRandomString(n int) string {
	str := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	bytes := []byte(str)
	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < n; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}

func GetAuthenticationByUserID(userID int) (auth *model.Authentication, err error) {
	sqlStr := "select uid, authentication_code from user_authentication_code where uid = ?"
	row := utils.CombineDb.QueryRow(sqlStr, userID)
	auth = &model.Authentication{}
	err = row.Scan(&auth.UserID, &auth.Code)
	if err != nil {
		return nil, err
	}
	return auth, nil
}
func GetAuthenticationByCode(code string) (auth *model.Authentication, err error) {
	sqlStr := "select uid, authentication_code from user_authentication_code where authentication_code = ?"
	row := utils.CombineDb.QueryRow(sqlStr, code)
	auth = &model.Authentication{}
	err = row.Scan(&auth.UserID, &auth.Code)
	if err != nil {
		return nil, err
	}
	return auth, nil
}
