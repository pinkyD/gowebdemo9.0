package dao

import (
	"order_generate9.0/model"
	"order_generate9.0/utils"
)

func GetStudentByStudentID(studentID string) (stu *model.Student) {
	sqlStr := "select id, student_id, name, gender, mobile from user_information where student_id = ?"
	row := utils.CombineDb.QueryRow(sqlStr, studentID)
	stu = &model.Student{}
	err := row.Scan(&stu.UID, &stu.StudentID, &stu.Name, &stu.Gender, &stu.Mobile)
	if err != nil {
		return nil
	}
	return stu
}
func GetStudentByUID(uid int) (stu *model.Student, err error) {
	sqlStr := "select id, student_id, name, gender, mobile from user_information where id = ?"
	row := utils.CombineDb.QueryRow(sqlStr, uid)
	stu = &model.Student{}
	err = row.Scan(&stu.UID, &stu.StudentID, &stu.Name, &stu.Gender, &stu.Mobile)
	if err != nil {
		return nil, err
	}
	return stu, nil
}

func AddStudent(stu *model.Student) (err error) {
	sqlStr := "insert into user_information(student_id, name, gender, mobile) values(?,?,?,?)"
	_, err = utils.CombineDb.Exec(sqlStr, stu.StudentID, stu.Name, stu.Gender, stu.Mobile)
	if err != nil {
		return err
	}
	return nil
}

func UpdateStudent(stu *model.Student) (err error) {
	sqlStr := "update user_information set gender = ?, mobile = ?  where student_id = ? and name = ?"
	_, err = utils.CombineDb.Exec(sqlStr, stu.Gender, stu.Mobile, stu.StudentID, stu.Name)
	if err != nil {
		return err
	}
	return nil
}
