package dao

import (
	"order_generate9.0/model"
	"order_generate9.0/utils"
)

func AddStudentIntoDorm(studentID string, dormID int) (err error) {
	sqlStr := "insert into student_dorm(student_id, dorm_id) values(?,?)"
	_, err = utils.CombineDb.Exec(sqlStr, studentID, dormID)
	return err
}

func DeleteStudentFromDorm(studentID int) (err error) {
	sqlStr := "delete from student_dorm where student_id = ?"
	_, err = utils.CombineDb.Exec(sqlStr, studentID)
	return err
}

func GetStuDormsByDormID(dormID int) (stuDorms []*model.StudentDorm) {
	sqlStr := "select id, student_id, dorm_id from student_dorm where dorm_id = ?"
	rows, err := utils.CombineDb.Query(sqlStr, dormID)
	if err != nil {
		return nil
	}
	for rows.Next() {
		stuDorm := &model.StudentDorm{}
		err = rows.Scan(&stuDorm.ID, &stuDorm.StudentID, &stuDorm.DormID)
		if err != nil {
			return stuDorms
		}
		stuDorms = append(stuDorms, stuDorm)
	}
	return stuDorms
}

func GetStuDormByStudentID(studentID string) (stuDorm *model.StudentDorm) {
	sqlStr := "select id, student_id, dorm_id from student_dorm where student_id = ?"
	stuDorm = &model.StudentDorm{}
	row := utils.CombineDb.QueryRow(sqlStr, studentID)
	err := row.Scan(&stuDorm.ID, &stuDorm.StudentID, &stuDorm.DormID)
	if err != nil {
		return nil
	}
	return stuDorm
}
