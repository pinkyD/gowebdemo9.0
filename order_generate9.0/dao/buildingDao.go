package dao

import (
	"order_generate9.0/model"
	"order_generate9.0/utils"
)

func GetBuildingByName(name string) (building *model.Building, err error) {
	sqlStr := "select building_id, building_name from building where building_name = ?"
	row := utils.CombineDb.QueryRow(sqlStr, name)
	building = &model.Building{}
	err = row.Scan(&building.ID, &building.Name)
	if err != nil {
		return nil, err
	}
	return building, nil
}

func GetBuildingByID(ID int) (building *model.Building) {
	sqlStr := "select building_id, building_name from building where building_id = ?"
	row := utils.CombineDb.QueryRow(sqlStr, ID)
	building = &model.Building{}
	err := row.Scan(&building.ID, &building.Name)
	if err != nil {
		return nil
	}
	return building
}
