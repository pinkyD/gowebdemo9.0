package model

// 在定义与JSON 相配套的 struct 时需要注意 json 描述必须与JSON 数据里面的 key 相同，否则 数据会被解析成空值
type OrderInfo struct {
	BuildingName  string `json:"building_name"`
	// CommitterCode string `json:"committer_code"`
	Roomate1Code  string `json:"roomate1_code"`
	Roomate2Code  string `json:"roomate2_code"`
	Roomate3Code  string `json:"roomate3_code"`
	Roomate4Code  string `json:"roomate4_code"`
}
