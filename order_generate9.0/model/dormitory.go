package model

type Building struct {
	ID   int
	Name string
}

type Unit struct {
	ID         int
	BuildingID int
	Name       string
}

type Dorm struct {
	ID                int
	UnitID            int
	Name              string
	Gender            string
	TotalBedCount     int
	AvailableBedCount int
	BrokenBedCount    int
}
