package model

import "github.com/dgrijalva/jwt-go"

type Claims struct {
	UserID    int
	StudentID string
	// Name      string
	jwt.StandardClaims
}
