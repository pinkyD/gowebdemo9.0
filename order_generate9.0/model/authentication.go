package model

type Authentication struct {
	UserID    int
	Code      string `json:"authentication_code"`
}
