package model

type Student struct {
	UID       int    `json:"uid"`
	StudentID string `json:"student_id"`
	Name      string `json:"name"`
	Gender    string `json:"gender"`
	Mobile    string `json:"mobile"`
}
