package model

// 订单表
type Order struct {
	ID           int `json:"order_id"`
	CommitterID  string
	BuildingID   int
	Gender       string
	StudentCount int
	IsSuccess    bool
	CommitTime   string
}

// 订单详情表
type OrderItem struct {
	ID        int `json:"item_id"`
	OrderID   int
	StudentID string
}
