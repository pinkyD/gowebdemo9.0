-- 建库
CREATE DATABASE IF NOT EXISTS combine_db default charset utf8 COLLATE utf8_general_ci;

-- 切换数据库
use combine_db;

-- ----------------------------
-- Table structure for student_dorm
-- ----------------------------
DROP TABLE IF EXISTS `student_dorm`;
CREATE TABLE `student_dorm`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dorm_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;
