-- 建库
CREATE DATABASE IF NOT EXISTS combine_db default charset utf8 COLLATE utf8_general_ci;

-- 切换数据库
use combine_db;

-- ----------------------------
-- Table structure for building
-- ----------------------------
DROP TABLE IF EXISTS `building`;
CREATE TABLE `building`  (
  `building_id` int(11) NOT NULL AUTO_INCREMENT,
  `building_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`building_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of building
-- ----------------------------
INSERT INTO `building` VALUES (1, 'Building 5');
INSERT INTO `building` VALUES (2, 'Building 8');
INSERT INTO `building` VALUES (3, 'Building 9');
INSERT INTO `building` VALUES (4, 'Building 13');
INSERT INTO `building` VALUES (5, 'Building 14');


-- ----------------------------
-- Table structure for unit
-- ----------------------------
DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit`  (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `building_id` int(11) NULL DEFAULT NULL,
  `unit_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`unit_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of unit
-- ----------------------------
INSERT INTO `unit` VALUES (1, 1, 'Building 5 Floor 1');
INSERT INTO `unit` VALUES (2, 1, 'Building 5 Floor 2');
INSERT INTO `unit` VALUES (3, 2, 'Building 8 Floor 1');
INSERT INTO `unit` VALUES (4, 2, 'Building 8 Floor 2');
INSERT INTO `unit` VALUES (5, 3, 'Building 9 Floor 1');
INSERT INTO `unit` VALUES (6, 3, 'Building 9 Floor 2');
INSERT INTO `unit` VALUES (7, 4, 'Building 13 Floor 1');
INSERT INTO `unit` VALUES (8, 4, 'Building 13 Floor 2');
INSERT INTO `unit` VALUES (9, 5, 'Building 14 Floor 1');
INSERT INTO `unit` VALUES (10, 5, 'Building 14 Floor 2');


-- ----------------------------
-- Table structure for dorm
-- ----------------------------
DROP TABLE IF EXISTS `dorm`;
CREATE TABLE `dorm`  (
  `dorm_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_id` int(11) NULL DEFAULT NULL,
  `dorm_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `total_bed_count` int(11) NULL DEFAULT NULL,
  `available_bed_count` int(11) NULL DEFAULT NULL,
  `broken_bed_count` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`dorm_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dorm
-- ----------------------------
INSERT INTO `dorm` VALUES (1, 1, '5101', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (2, 1, '5102', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (3, 1, '5103', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (4, 1, '5104', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (5, 1, '5105', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (6, 1, '5106', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (7, 1, '5107', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (8, 1, '5108', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (9, 1, '5109', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (10, 1, '5110', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (11, 2, '5201', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (12, 2, '5202', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (13, 2, '5203', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (14, 2, '5204', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (15, 2, '5205', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (16, 2, '5206', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (17, 2, '5207', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (18, 2, '5208', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (19, 2, '5209', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (20, 2, '5210', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (21, 3, '8101', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (22, 3, '8102', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (23, 3, '8103', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (24, 3, '8104', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (25, 3, '8105', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (26, 3, '8106', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (27, 3, '8107', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (28, 3, '8108', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (29, 3, '8109', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (30, 3, '8110', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (31, 4, '8201', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (32, 4, '8202', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (33, 4, '8203', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (34, 4, '8204', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (35, 4, '8205', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (36, 4, '8206', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (37, 4, '8207', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (38, 4, '8208', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (39, 4, '8209', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (40, 4, '8210', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (41, 5, '9101', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (42, 5, '9102', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (43, 5, '9103', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (44, 5, '9104', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (45, 5, '9105', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (46, 5, '9106', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (47, 5, '9107', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (48, 5, '9108', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (49, 5, '9109', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (50, 5, '9110', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (51, 6, '9201', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (52, 6, '9202', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (53, 6, '9203', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (54, 6, '9204', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (55, 6, '9205', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (56, 6, '9206', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (57, 6, '9207', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (58, 6, '9208', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (59, 6, '9209', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (60, 6, '9210', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (61, 7, '13101', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (62, 7, '13102', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (63, 7, '13103', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (64, 7, '13104', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (65, 7, '13105', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (66, 7, '13106', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (67, 7, '13107', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (68, 7, '13108', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (69, 7, '13109', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (70, 7, '13110', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (71, 8, '13201', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (72, 8, '13202', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (73, 8, '13203', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (74, 8, '13204', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (75, 8, '13205', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (76, 8, '13206', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (77, 8, '13207', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (78, 8, '13208', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (79, 8, '13209', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (80, 8, '13210', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (81, 9, '14101', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (82, 9, '14102', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (83, 9, '14103', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (84, 9, '14104', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (85, 9, '14105', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (86, 9, '14106', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (87, 9, '14107', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (88, 9, '14108', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (89, 9, '14109', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (90, 9, '14110', 'M', 4, 4, '0');
INSERT INTO `dorm` VALUES (91, 10, '14201', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (92, 10, '14202', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (93, 10, '14203', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (94, 10, '14204', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (95, 10, '14205', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (96, 10, '14206', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (97, 10, '14207', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (98, 10, '14208', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (99, 10, '14209', 'F', 4, 4, '0');
INSERT INTO `dorm` VALUES (100, 10, '14210', 'F', 4, 4, '0');
